


function [tempVec] = simPopulation(tempVec,modeVec,s_in)

    %advance temperature dynamics once:
        tempVec = s_in.A.*tempVec+ (1-s_in.A).*(s_in.Ta - modeVec.*s_in.R.*s_in.P) + 0*sqrt(s_in.var)*randn(size(modeVec));
        
end