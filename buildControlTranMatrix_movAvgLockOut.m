function PtranCycle = buildControlTranMatrix_movAvgLockOut(Pcf,pSwitch_On2Off,pSwitch_Off2On,p)

    
%get individual matrix blocks:
    s_out = buildMatBlocks_movAvgLockOut_Sparse(Pcf,p);
    
    polGS_off2off = diag(1-pSwitch_Off2On);
    polGS_on2on = diag(1-pSwitch_On2Off);
    polGS_off2on = diag(pSwitch_Off2On);
    polGS_on2off = diag(pSwitch_On2Off);
    
    SOn = s_out.P_On2Off;
    SOff = s_out.P_Off2On;
    POff = s_out.P_Off2Off;
    POn = s_out.P_On2On;
    
%the degenerate policies:
    degPol_Off2On = zeros(size(pSwitch_Off2On)); degPol_Off2On(end) = 1;
    degPol_On2Off = zeros(size(pSwitch_Off2On)); degPol_On2Off(1) = 1;
    
%with lockout:
    CircTau = [zeros(p.tau-1,1),zeros(p.tau-1,1),eye(p.tau-1)];
    CircTau = [CircTau;1,zeros(1,p.tau)];
    eOne = zeros(1,p.tau+1); eOne(1) = 1;
    eTwo = zeros(1,p.tau+1); eTwo(2) = 1;
    CircTauTwo = repmat(eTwo,p.tau,1);
    
    
    tauBlock_Off2Off = [kron(eOne,polGS_off2off*POff); kron(CircTau,diag(1-degPol_Off2On)*POff)];
    
    tauBlock_On2On = [kron(eOne,polGS_on2on*POn); kron(CircTau,diag(1-degPol_On2Off)*POn)];
    
    tauBlock_Off2On = [kron(eTwo,polGS_off2on*SOff); kron(CircTauTwo,diag(degPol_Off2On)*SOff)];
    
    tauBlock_On2Off = [kron(eTwo,polGS_on2off*SOn); kron(CircTauTwo,diag(degPol_On2Off)*SOn)];
    
    PtranCycle = [tauBlock_Off2Off,tauBlock_Off2On; ...
                  tauBlock_On2Off, tauBlock_On2On];
              
                  
end