%Austin Coffman
%build transition matrix:

function [PtranDT,PrateCT,Ptran] = buildTranMatrix_withBuffer(p)
        
%extract parameters:
    Tmin = p.Tmin;
    Tmax = p.Tmax;
    Tlow = p.Tlow;
    Thigh = p.Thigh;
    deltaT = p.deltaT;
    numCVs = p.numCVs+1;
    numBuffers = p.numBuffers;
    sigmaSq = p.sigmaSq;
    deltaTime = p.deltaTime;
    
%build matrix:
    %convection:
        rightEdgesOn = ((Tmin - deltaT/2):deltaT:(Thigh - deltaT/2));
%         rightEdgesOn = rightEdgesOn*0 + p.tempSet;
        tempOnDiag = -(1/(p.Cth*p.Rth))*(rightEdgesOn-p.thetaA) - 1*p.rateP/p.Cth;
        tempOnSupDiag = -(1/(p.Cth*p.Rth))*(rightEdgesOn(2:end)-p.thetaA) - 1*p.rateP/p.Cth;
        AonDiag = diag(tempOnDiag);
        AonSupDiag = [zeros(numCVs-1,1),diag(tempOnSupDiag)];
        AonSupDiag = [AonSupDiag; zeros(1,numCVs)];
        Aon = AonDiag - AonSupDiag;
        Aon(1,1) = -deltaT/deltaTime;
        
        leftEdgesOff = ((Tlow + deltaT/2):deltaT:(Tmax + deltaT/2));
%         leftEdgesOff = leftEdgesOff*0 + p.tempSet;
        tempOffDiag = -(1/(p.Cth*p.Rth))*(leftEdgesOff-p.thetaA) - 0*p.rateP/p.Cth;
        tempOffSubDiag = -(1/(p.Cth*p.Rth))*(leftEdgesOff(1:end-1)-p.thetaA) - 0*p.rateP/p.Cth;
        AoffDiag = -diag(tempOffDiag);
        AoffSubDiag = [zeros(1,numCVs-1);diag(tempOffSubDiag)];
        AoffSubDiag = [AoffSubDiag,zeros(numCVs,1)];
        Aoff = AoffDiag + AoffSubDiag;
        Aoff(end,end) = -deltaT/deltaTime;
        
        %add in thermostat switching:
        A = blkdiag(Aoff,Aon);
        A(numBuffers,numCVs+1) = deltaT/deltaTime;
        numCVs_DB = numCVs - numBuffers;
        A(numCVs+numCVs_DB+1,numCVs) = deltaT/deltaTime;
    
    %diffusion:
        Adiff = (-sigmaSq/deltaT^2)*eye(numCVs);
        ADiffSupDiag = [zeros(numCVs-1,1),(sigmaSq/deltaT^2)*eye(numCVs-1)./2];
        ADiffSupDiag = [ADiffSupDiag;zeros(1,numCVs)];
        ADiffSubDiag = [zeros(1,numCVs-1);(sigmaSq/deltaT^2)*eye(numCVs-1)./2];
        ADiffSubDiag = [ADiffSubDiag,zeros(numCVs,1)];
        Adiff = Adiff + ADiffSupDiag + ADiffSubDiag;
        Adiff = blkdiag(Adiff,Adiff);
        Adiff(1,1) = Adiff(1,1)/2;
        Adiff(end,end) = Adiff(end,end)/2;
        Adiff(numCVs,numCVs) = 0; Adiff(numCVs-1,numCVs) = 0;
        Adiff(numCVs+1,numCVs+1) = 0; Adiff(numCVs+2,numCVs+1) = 0;
%         Adiff(numBuffers,numCVs+1) = (sigmaSq/deltaT^2)/2;
%         Adiff(numCVs+numBuffers+2,numCVs) = (sigmaSq/deltaT^2)/2;
            
    %cont time matrix:
        PrateCT = A'./deltaT + Adiff';
        
    %discrete time matrix:
        PtranDT = eye(size(PrateCT)) + deltaTime*PrateCT;

%     %policy for thermostat control:
%         rhoOn = PtranDT(numCVs+1,numBuffers+1)./(PtranDT(numCVs+1,numBuffers+1) + PtranDT(numCVs+1,numCVs+1));
%         rhoOff = PtranDT(numCVs,numCVs+numBuffers+1)./(PtranDT(numCVs,numCVs+numBuffers+1) + PtranDT(numCVs,numCVs));        
%         
    %compute control free tran matrix (to be merged with policy):
        Ptran = PtranDT;
        Ptran(numCVs+1,numCVs+1) = PtranDT(numCVs+1,numBuffers) + PtranDT(numCVs+1,numCVs+1);
        Ptran(numCVs,numCVs) = PtranDT(numCVs,numCVs+numCVs_DB+1) + PtranDT(numCVs,numCVs);
        Ptran(numCVs+1,numBuffers) = 0;
        Ptran(numCVs,numCVs+numCVs_DB+1) = 0;
    
        
end












