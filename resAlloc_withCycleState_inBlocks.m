%Austin Coffman
%test convex resource allocation problem:

%clear interface:
    clear
    close all
    clc
    
%data saving parameters:
    saveData = 1;
    
%define parameters for discretization:
    %finite volume method parameters:
        p.numDB_CVs = 10;
        p.Tmax = 22; p.Tmin = 20;
        p.deltaT = (p.Tmax - p.Tmin)/p.numDB_CVs;
        p.numBuffers = 2;%(p.Thigh - p.Tmax)/p.deltaT;
        p.Tlow = p.Tmin - p.deltaT*p.numBuffers; 
        p.Thigh = p.Tmax + p.deltaT*p.numBuffers;
        p.numCVs = p.numDB_CVs + p.numBuffers;        
        p.deltaTime = (1)*60/3600;
   
    %create policy upperbound:
        p.UB = zeros(p.numDB_CVs,1);
        indOne = floor((p.numDB_CVs - 1)/2);
        p.UB(1:indOne) = 0.025;
        p.UB(indOne+1:p.numDB_CVs-1) = 1;
        
    %TCL modeling parameters:
        p.Cth = 2;
        p.Rth = 1;
        p.thetaA = 23.66;
        p.eta = 1;
        p.tempSet = (p.Tmax - p.Tmin)./2 + p.Tmin;
        p.rateP = (35 - p.tempSet)./p.Rth;
        p.sigmaSq = 0.025;     
        
        %compute max-allowable stepsize:
        rightEdgesOn = ((p.Tmin - p.deltaT/2):p.deltaT:(p.Thigh - p.deltaT/2));
        fOn = -(1/(p.Cth*p.Rth))*(rightEdgesOn-p.thetaA) - 1*p.rateP/p.Cth;
        minOn = min(2*p.deltaT./(abs(fOn) + p.sigmaSq/p.deltaT));
        
        leftEdgesOff = ((p.Tlow + p.deltaT/2):p.deltaT:(p.Tmax + p.deltaT/2));
        fOff = -(1/(p.Cth*p.Rth))*(leftEdgesOff-p.thetaA) - 0*p.rateP/p.Cth;
        minOff = min(2*p.deltaT./(abs(fOff) + p.sigmaSq/p.deltaT));
        
        maxDeltaT = min(minOff,minOn);
        
        [Ptran,Arate,Pcf] = buildTranMatrix_withBuffer(p);
        
    %reference signal properties:
        numDays = 1;
        numBlocks = 12;
        numHours = 24*numDays;
        Ts = p.deltaTime; %units: hour
        tHour = 0:Ts:numHours;
        nTime = length(tHour);
        blockLength = (nTime-1)/numBlocks;
        p.nTime = blockLength;
        
    %ambient temperature:
        s_in.nPlan = 1;
        TsTa = 5*60/3600;
        tHourTa = 0:TsTa:numHours;
        TaRaw = designTA('WU',length(tHourTa),s_in,0,0,1,1);
        Ta = interp1(tHourTa,TaRaw(1:end-1),tHour);
        yStar = (Ta-p.tempSet)./(p.Rth*p.rateP);
        refSignal = yStar' + 0.05*sin(7*tHour') + 0.13*sin(1.5*tHour') + 0.07*sin(3*tHour');   

    %aggregate model C matrix:
        Con = [zeros(p.numCVs+1,1);ones(p.numCVs+1,1)]; p.Con = Con;
        
    %aggregate model initial condition:  
        p.tau = 5;
        nuZero = rand(p.tau+1,length(Con));
        nuZero = 0*nuZero./sum(nuZero);
        nuZero(1,p.numBuffers+4:p.numCVs+1) = 4/p.numCVs;
        nuZero(1,p.numCVs+5:2*(p.numCVs+1)-p.numBuffers) = 4/p.numCVs;
        nuZero(1,:) = nuZero(1,:)./sum(nuZero(1,:));
        nuZeroHat = nuZero;
                 
    %load in nominal marginals:
        margLoad = load('margNomRand.mat');
        nomMarg = margLoad.nuSave;
        
%prealloc:
    p.bSize = p.numCVs+1;
    polOff2OnSave = zeros(p.bSize,nTime-1);
    polOn2OffSave = zeros(p.bSize,nTime-1);
    gammaSave = zeros(1,nTime-1);
    
%moving horizon implementation:    
    for jj=1:1:numBlocks      

        %set moving horizon block indicies:           
            indexOne = blockLength*(jj-1) + 1;
            indexTwo = blockLength*jj;

        %set input data:
            TaBlock = Ta(indexOne:indexTwo);
            refSignalBlock = refSignal(indexOne:indexTwo+1);

        %send in nom marg:
%             tempNomMargOn = nomMarg(indexOne:indexTwo,(p.tau+1)*(p.numCVs+1)+1:end);
%             tempNomMargOnRS = reshape(tempNomMargOn',(p.numCVs+1),blockLength*(p.tau+1))';
%             p.nomMargOn = reshape(tempNomMargOnRS',1,(p.numCVs+1)*blockLength*(p.tau+1)); 
%             
%             tempNomMargOff = nomMarg(indexOne:indexTwo,1:(p.tau+1)*(p.numCVs+1));
%             tempNomMargOffRS = reshape(tempNomMargOff',(p.numCVs+1),blockLength*(p.tau+1))';
%             p.nomMargOff = reshape(tempNomMargOffRS',1,(p.numCVs+1)*blockLength*(p.tau+1)); 
            
        %compute policy for current horizon:
            [polOff2OnTemp,polOn2OffTemp,nuZero,gammaOut] = solveResAlloc_withCVX_withCycle_v2(refSignalBlock, TaBlock, nuZero, p);

        %save policies and aggregate model output:
            gammaSave(indexOne:indexTwo) = gammaOut;
            polOff2OnSave(:,indexOne:indexTwo) = polOff2OnTemp;
            polOn2OffSave(:,indexOne:indexTwo) = polOn2OffTemp;

    end

%save the data for use in simulation:
    if saveData
        nuZero = nuZeroHat;
        save('optimalPolicies_ProblemData_3.mat','polOn2OffSave','polOff2OnSave','p',...
            'numDays','numHours','Ts','tHour','nTime','Ta','refSignal','nuZero','gammaSave');
    end
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    
    