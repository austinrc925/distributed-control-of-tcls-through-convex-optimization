function s_out = buildMatBlocks_movAvgLockOut_Sparse(Pcf,p)

    [m,n] = size(Pcf);
    numBuffers = p.numBuffers;
    
    PblockOne = Pcf(1:m/2,1:m/2);
    
    flippedUpperBlock = [Pcf(numBuffers:m/2,numBuffers:m/2),zeros(m/2-numBuffers+1,numBuffers-1)];
    flippedUpperBlock = [zeros(numBuffers-1,m/2);flippedUpperBlock];
%     flippedUpperBlock = [zeros(m/2-numBuffers+1,numBuffers-1),Pcf(numBuffers:m/2,numBuffers:m/2)];
%     flippedUpperBlock = [flippedUpperBlock;zeros(numBuffers-1,m/2)];
    PblockTwo = flippedUpperBlock;
    
    PblockThree = Pcf(m/2+1:end,m/2+1:end);
    
    flippedLowerBlock = [zeros(m/2-numBuffers+1,numBuffers-1),Pcf(m/2+1:end-numBuffers+1,m/2+1:end-numBuffers+1)];
    flippedLowerBlock = [flippedLowerBlock;zeros(numBuffers-1,m/2)];
    PblockFour = flippedLowerBlock;
    
%the degenerate policies:
    degPol_Off2On = zeros(m/2,1); degPol_Off2On(end) = 1;
    degPol_On2Off = zeros(m/2,1); degPol_On2Off(1) = 1;
    
%the 8 sub matrices require:
    s_out.P_Off2Off_TS = sparse((1-degPol_Off2On).*PblockOne);
%     s_out.P_Off2On_TS = sparse(degPol_Off2On.*PblockTwo);
    s_out.P_Off2On_TS = sparse(degPol_Off2On.*PblockOne);
    s_out.P_On2On_TS = sparse((1-degPol_On2Off).*PblockThree);
%     s_out.P_On2Off_TS = sparse(degPol_On2Off.*PblockFour);
    s_out.P_On2Off_TS = sparse(degPol_On2Off.*PblockThree);

    s_out.P_Off2Off = sparse(PblockOne);
    s_out.P_Off2On = sparse(PblockTwo);
    s_out.P_On2Off = sparse(PblockFour);
    s_out.P_On2On = sparse(PblockThree);
    
end







