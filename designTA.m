%Austin Coffman
%Function to design ambient temperature for MPC  simulations:
%
%
%
%
%FUNCTION
function [Ta_k,qOccup_k,TaHist] = designTA(inputLevel,nTotal,s_in,tHourExt,loadHetero,numDevices,sidToolBox)
    

    if strcmp(inputLevel,'sumSines')
        days = 20;
        omega = 2*pi/12;%*pi/(24*days);
        omega1 = 2*pi/24;
        omega2 = 0.04;
        omega3 = 0.52;
        Ta_k = 30.2*ones(nTotal+s_in.nPlan,1) + 1.2760*sin(omega1*tHourExt)' + 0.4560*sin(omega2*tHourExt)' + 0.1280*sin(omega3*tHourExt)';
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1)';
    elseif strcmp(inputLevel,'singleSine')
        omega = (2*pi/24); %1/day
        Ta_k = 27 + 6*sin(omega*tHourExt)';
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1)';
    elseif strcmp(inputLevel,'dayag')
        desired_start_date = '2013-08-13';
        [outside_temp] = func_getnewdayagdata('wunder.temperature',desired_start_date,14);
        T_a = outside_temp.unifsampleddata;
        Ta_k = 1*func_f2c(T_a(1:nTotal+s_in.nPlan));
%         Ta_k = Ta_k + 23 - min(Ta_k);
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1);
    elseif strcmp(inputLevel,'filtered')
        nyqFreq = (1/300)/2;
        freqBand1 = 1/(3600*40); %1/1 hour
        freqBand2 = 1/(3600*20); %1/ 15 minutes
        if sidToolBox
            TaRand = idinput(nTotal+s_in.nPlan,'prbs',[0 1/7],[-3,9]);
        else
            randLoad = load('TaRandSave.mat');
            TaRand = randLoad.TaRand(1:nTotal+s_in.nPlan);
        end
        TaRand = BPA_LowPass_SPM(TaRand, 1, [freqBand1 freqBand2]./nyqFreq);
        TaRand = TaRand - mean(TaRand);
        Ta_k = 28 + TaRand;
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1);
    elseif strcmp(inputLevel,'Constant')
        %         Ta_k = [26*ones((nTotal+s_in.nPlan)/3,1);34*ones((nTotal+s_in.nPlan)/3,1);26*ones((nTotal+s_in.nPlan)/3,1)];
        Ta_k = 30*ones((nTotal+s_in.nPlan),1);
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1);
    elseif strcmp(inputLevel,'WU')
        data = csvread('tempSaveSummer.csv',1,1);
%         data = csvread('springData.csv',1,1);
        data = data(1:nTotal+s_in.nPlan,:);
        counterFlag = 1;
        flagData = zeros(size(data,2),1);
        for i=1:1:size(data,2)
            if (min(data(:,i)) < 0 && max(data(:,i)) > func_c2f(5))
                indBad = find(data(:,i)<0);
                counter = 1;
                counterOne = 1;
                for j=1:1:length(indBad)
                    if ~(j == length(indBad))
                        nextIndex = indBad(j) + 1;
                        if nextIndex == indBad(j+1)
                            counter = counter + 1;
                        else
                            data(indBad(counterOne:counterOne+counter-1),i) = data(indBad(counterOne)-1,i);
                            counterOne = counterOne + counter;
                            counter = 1;
                        end
                    else
                        data(indBad(counterOne:end),i)  = data(indBad(counterOne)-1,i);
                    end
                end
                data(:,i) = func_f2c(data(:,i));
            else
                if (max(data(:,i)) < func_c2f(5))
                    flagData(counterFlag) = i;
                    counterFlag = counterFlag + 1;
                else
                    data(:,i) = 1.*func_f2c(data(:,i));
                end
            end            
        end
        data(:,flagData(1:counterFlag-1)) =  [];
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1);
%         data(:,3) = [];
    elseif strcmp(inputLevel,'stepInput')
        indHalf = round((nTotal+s_in.nPlan)/2);
        Ta_first = 26*ones(indHalf,1);
        Ta_second = 19*ones((nTotal+s_in.nPlan)-indHalf,1);
        Ta_k = [Ta_first, Ta_second];
        qOccup_k = 0*ones(nTotal+s_in.nPlan,1);
    end
        
%add heterogeneity if desired:        
    qOccup_k = qOccup_k(:)';%make row
    TaRand = zeros(numDevices,nTotal+s_in.nPlan);
    if strcmp(inputLevel,'WU')
        for i=1:1:numDevices
            alpha = rand(size(data,2),1);
            alphaNorm = ones(size(alpha))./size(data,2);
            TaHist(i,:) = (data(1:nTotal+s_in.nPlan,:)*alphaNorm)';
        end
        Ta_k = mean(TaHist,1);
        if ~loadHetero
            TaHist = repmat(Ta_k(:)',numDevices,1);
        end
        
    
    elseif loadHetero
        Ta_k = Ta_k(:)'; %make row
        
        if exist('TaRand_RGS2.mat') == 0
            for i=1:1:numDevices
                TaRand(i,:) = idinput(nTotal+s_in.nPlan,'rgs',[0 1/50],[-0.4,0.4])';
            end
        elseif exist('TaRand_RGS2.mat') == 2
            TaRandLoad = load('TaRand_RGS.mat');
            TaRand = TaRandLoad.TaRand(1:numDevices,1:nTotal+s_in.nPlan);
        end
        TaHist = repmat(Ta_k,numDevices,1) + TaRand;
    end
    if ~strcmp(inputLevel,'WU') && ~loadHetero
        Ta_k = Ta_k(:)';
        TaHist = repmat(Ta_k,numDevices,1) + TaRand;
    end
    

end