% function to convert temperature in Fahrenheit to Celsius
% syntax:
%
% function c = func_f2c(f)
%
% 1 input: f - temperature in F, can be scalar or vector
% 1 output: c - temperature in Celcius, same size as input
%
% ver 1: Prabir Barooah

function c = func_f2c(f)
c = (f-32)*5./9;
