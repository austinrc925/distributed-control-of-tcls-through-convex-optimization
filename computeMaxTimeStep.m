function [maxDeltaT,maxDeltaVec] = computeMaxTimeStep(p,TaMin,TaMax)
        
        thetaAVec = linspace(TaMin,TaMax,100);
        maxDeltaVec = zeros(1,100);
        for i=1:1:length(thetaAVec)
            p.thetaA = thetaAVec(i);
            rightEdgesOn = ((p.Tmin - p.deltaT/2):p.deltaT:(p.Thigh - p.deltaT/2));
            fOn = -(1/(p.Cth*p.Rth))*(rightEdgesOn-p.thetaA) - 1*p.rateP/p.Cth;
            minOn = min(p.deltaT./(abs(fOn) + p.sigmaSq/p.deltaT));

            leftEdgesOff = ((p.Tlow + p.deltaT/2):p.deltaT:(p.Tmax + p.deltaT/2));
            fOff = -(1/(p.Cth*p.Rth))*(leftEdgesOff-p.thetaA) - 0*p.rateP/p.Cth;
            minOff = min(p.deltaT./(abs(fOff) + p.sigmaSq/p.deltaT));
            maxDeltaVec(i) = min(minOff,minOn);
        end
        maxDeltaT = min(maxDeltaVec);

end