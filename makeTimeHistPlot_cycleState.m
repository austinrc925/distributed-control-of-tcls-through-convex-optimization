function figHist = makeTimeHistPlot_cycleState(s_in)
        

        %destruct input:
        BinCentersOn = s_in.BinCentersOn;
        BinEdgesOn = s_in.BinEdgesOn;
        numDevices = s_in.numDevices;
        
        nuPlotOne = s_in.nuPlotOn.plot_1;
        nuPlotTwo = s_in.nuPlotOn.plot_2;
        nuPlotThree = s_in.nuPlotOn.plot_3;
        nuPlotFour = s_in.nuPlotOn.plot_4;
        nuPlotFive = s_in.nuPlotOn.plot_5;
        
        stateVectorOne = s_in.stateVectorOnSave.plot_1;
        stateVectorTwo = s_in.stateVectorOnSave.plot_2;
        stateVectorThree = s_in.stateVectorOnSave.plot_3;
        stateVectorFour = s_in.stateVectorOnSave.plot_4;
        stateVectorFive = s_in.stateVectorOnSave.plot_5;
        
        onShiftValue = 4.5;
        figHist = figure;
        axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
        axes(axA)
        %make the patches:
        yMax = 1.1*max([max(nuPlotOne*numDevices); ...
                    max(nuPlotTwo*numDevices); ...
                    max(nuPlotThree*numDevices); ...
                    max(nuPlotFour*numDevices); ...
                    max(nuPlotFive*numDevices)]);
        LB = zeros(size(BinCentersOn));
        UB = yMax*ones(size(BinCentersOn));
        
        hPatchOff = patch([BinCentersOn , fliplr(BinCentersOn)], [LB, fliplr(UB)], 'b', 'FaceAlpha',0.075);
        hPatchOff.EdgeColor = [1,1,1];
        hPatchOff.EdgeAlpha = 0;
        
        hPatchOn = patch([BinCentersOn+onShiftValue , fliplr(BinCentersOn+onShiftValue)], [LB, fliplr(UB)], 'm', 'FaceAlpha',0.075);
        hPatchOn.EdgeColor = [1,1,1];
        hPatchOn.EdgeAlpha = 0;
        
        hPatchOn = patch([BinCentersOn+2*onShiftValue , fliplr(BinCentersOn+2*onShiftValue)], [LB, fliplr(UB)], 'b', 'FaceAlpha',0.075);
        hPatchOn.EdgeColor = [1,1,1];
        hPatchOn.EdgeAlpha = 0;
        
        hPatchOn = patch([BinCentersOn+3*onShiftValue , fliplr(BinCentersOn+3*onShiftValue)], [LB, fliplr(UB)], 'm', 'FaceAlpha',0.075);
        hPatchOn.EdgeColor = [1,1,1];
        hPatchOn.EdgeAlpha = 0;
            
        hPatchOn = patch([BinCentersOn+4*onShiftValue , fliplr(BinCentersOn+4*onShiftValue)], [LB, fliplr(UB)], 'b', 'FaceAlpha',0.075);
        hPatchOn.EdgeColor = [1,1,1];
        hPatchOn.EdgeAlpha = 0;
        
        hold on;
        
        H1 = histogram(stateVectorOne,BinEdgesOn);
        H1.FaceColor = [1 0 0];
        H1.FaceAlpha = 0.15;
        
        %hist 2:
        H2 = histogram(stateVectorTwo+onShiftValue,BinEdgesOn+onShiftValue);
        H2.FaceColor = [1 0 0];
        H2.FaceAlpha = 0.15;
        
        %hist 3:
        H3 = histogram(stateVectorThree+2*onShiftValue,BinEdgesOn +2*onShiftValue);
        H3.FaceColor = [1 0 0];
        H3.FaceAlpha = 0.15;
        
        %hist 4:
        H4 = histogram(stateVectorFour+3*onShiftValue,BinEdgesOn+3*onShiftValue);
        H4.FaceColor = [1 0 0];
        H4.FaceAlpha = 0.15;
        
        %hist 5:
        H4 = histogram(stateVectorFive+4*onShiftValue,BinEdgesOn+4*onShiftValue);
        H4.FaceColor = [1 0 0];
        H4.FaceAlpha = 0.15;
        
        %plot marginals:
        plot(BinCentersOn,nuPlotOne*numDevices,'g','LineWidth',1.75);
        plot(BinCentersOn+onShiftValue,nuPlotTwo*numDevices,'g','LineWidth',1.75);
        plot(BinCentersOn+2*onShiftValue,nuPlotThree*numDevices,'g','LineWidth',1.75);
        plot(BinCentersOn+3*onShiftValue,nuPlotFour*numDevices,'g','LineWidth',1.75);
        plot(BinCentersOn+4*onShiftValue,nuPlotFive*numDevices,'g','LineWidth',1.75);
        
        
%         xticks([linspace(18,22,5),linspace(20,24,5)+onShiftValue])
        xticklabels([]);
        xlim([20,24+4*onShiftValue])
        textStartVal = 20.75;
        textYval = UB(1)*1.1;
        text(textStartVal,textYval,'5 hr','FontSize',14,'FontWeight','Bold','FontName','Times');
        text(textStartVal+onShiftValue,textYval,'10 hr','FontSize',14,'FontWeight','Bold','FontName','Times');
        text(textStartVal+2*onShiftValue,textYval,'15 hr','FontSize',14,'FontWeight','Bold','FontName','Times');
        text(textStartVal+3*onShiftValue,textYval,'20 hr','FontSize',14,'FontWeight','Bold','FontName','Times');
        text(textStartVal+4*onShiftValue,textYval,'24 hr','FontSize',14,'FontWeight','Bold','FontName','Times');
%         text(textStartVal,textYval*1.2,['\sigma^2 = ',num2str(s_in.sigmaSq)],'FontSize',14,'FontWeight','Bold','FontName','Times');
        ylim([0,yMax*1.25]);
        ylabel('Count');
        xlabel('Temperature ^\circ C')
        set(gca,'FontSize',12,'FontWeight','Bold','FontName','Times');

end