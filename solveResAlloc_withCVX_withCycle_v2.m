
function [polOff2OnSave,polOn2OffSave,nuZero,gammaOut,nuMatOn,C_R] = solveResAlloc_withCVX_withCycle_v2(refSignalBlock, Ta, nuZero, p)
    

    %dimensions for problem below:
        N = (p.numCVs + 1)*(p.tau+1);
        M = p.numCVs+1;
        nuZeroOff = nuZero(:,1:M);
        nuZeroOn = nuZero(:,M+1:2*M);
        
        forDiffMat = eye(M) - [zeros(M-1,1),eye(M-1);zeros(1,M)];
        forDiffMat = forDiffMat(1:end-1,:);
        forDiffMatKron = sparse(kron(eye(p.nTime),forDiffMat'));
        
    %build matrices:
                C_R = sparse(p.nTime,N*p.nTime);
                indThrmPol_Off2On = zeros(1,p.nTime);
                indThrmPol_On2Off = zeros(1,p.nTime);
                for tt=1:1:p.nTime
                
                    indThrmPol_Off2On(tt) = M*tt;
                    indThrmPol_On2Off(tt) = M*(tt-1) + 1;
                    
                    rowVecTemp = zeros(1,p.nTime);
                    rowVecTemp(tt) = 1;
                    rowVecTempTwo = kron(rowVecTemp,ones(1,M));
                    C_R(tt,:) = repmat(rowVecTempTwo,1,p.tau+1);
                    
                   
                    
                %build required matrices:
                    %control free matrix blocks:
                        p.thetaA = Ta(tt);
                        [Ptran,Arate,Pcf] = buildTranMatrix_withBuffer(p);
                    %get blocks for constraints:
                        s_out = buildMatBlocks_movAvgLockOut_Sparse(Pcf,p);
                    %set to local environment variable names:
                        if tt == 1
                            P_Off2Off_TS = s_out.P_Off2Off_TS; 
                            P_Off2On_TS = s_out.P_Off2On_TS;
                            P_On2On_TS = s_out.P_On2On_TS;
                            P_On2Off_TS = s_out.P_On2Off_TS;

                            P_Off2Off = s_out.P_Off2Off;
                            P_Off2On = s_out.P_Off2On;
                            P_On2Off = s_out.P_On2Off;
                            P_On2On = s_out.P_On2On;
                            
                            
                            %indicies on/off
                            indPolDB_Off2On = 1:(p.numBuffers+3);
                            indPolDB_On2Off = (M - p.numBuffers-3):M;
                            
                            
                        end
                        
                        if tt > 1
                            P_Off2Off_TS = sparse(blkdiag(P_Off2Off_TS,s_out.P_Off2Off_TS)); 
                            P_Off2On_TS = sparse(blkdiag(P_Off2On_TS,s_out.P_Off2On_TS));
                            P_On2On_TS = sparse(blkdiag(P_On2On_TS,s_out.P_On2On_TS));
                            P_On2Off_TS = sparse(blkdiag(P_On2Off_TS,s_out.P_On2Off_TS));

                            P_Off2Off = sparse(blkdiag(P_Off2Off,s_out.P_Off2Off));
                            P_Off2On = sparse(blkdiag(P_Off2On,s_out.P_Off2On));
                            P_On2Off = sparse(blkdiag(P_On2Off,s_out.P_On2Off));
                            P_On2On = sparse(blkdiag(P_On2On,s_out.P_On2On));
                                                        
                            indPolDB_Off2On = [indPolDB_Off2On, (1:(p.numBuffers+3)) + M*(tt-1)];
                            indPolDB_On2Off = [indPolDB_On2Off, ((M - p.numBuffers-3):M) + M*(tt-1)];
                        end
                        
                end
                

%% solve with CVX:
    cvx_begin
        %set solver:
            cvx_solver SDPT3
            cvx_precision low
        %decision variable Joint Policy (starting in off):
            variable JointPol_Off2On(1,p.nTime*M)
            variable JointPol_Off2Off(1,p.nTime*M)
        %decision variable Joint Policy (starting in on):
            variable JointPol_On2Off(1,p.nTime*M)
            variable JointPol_On2On(1,p.nTime*M)
        %decision variable (marginal):
            variable nuMatOn(1,p.nTime*N)
            variable nuMatOff(1,p.nTime*N)
        %slack variables:
            variable On2OffSlack(1,size(forDiffMatKron,2))
            variable Off2OnSlack(1,size(forDiffMatKron,2))  
        %objective function:
            minimize( norm(refSignalBlock(1:end-1) - C_R*nuMatOn',2) + 0.5*sum(On2OffSlack) + 0.5*sum(Off2OnSlack) )
        %constraints:
        subject to
        
        %% required constraints:
        
        %in [0,1] box constraints:
            0 <= JointPol_Off2Off <= 1
            0 <= JointPol_Off2On <= 1
            0 <= JointPol_On2Off <= 1
            0 <= JointPol_On2On <= 1
            0 <= nuMatOn <= 1
            0 <= nuMatOff <= 1
            0 <= Off2OnSlack
            0 <= On2OffSlack
                    
        %sum joint equal marg:
            %off:
            nuZeroOff(1,:) - JointPol_Off2Off(1:M) - JointPol_Off2On(1:M) == 0
            nuMatOff(1:p.nTime*M-M) - JointPol_Off2Off(M+1:p.nTime*M) - JointPol_Off2On(M+1:p.nTime*M) == 0
            
            %on:
            nuZeroOn(1,:) - JointPol_On2Off(1:M) - JointPol_On2On(1:M) == 0
            nuMatOn(1:p.nTime*M-M) - JointPol_On2Off(M+1:p.nTime*M) - JointPol_On2On(M+1:p.nTime*M) == 0    
            
        %% non-required technical constraints:   
        
        %psuedo constraint for policies to increase:        
            -JointPol_Off2On*forDiffMatKron + Off2OnSlack >= 0
            JointPol_On2Off*forDiffMatKron + On2OffSlack >= 0

        %setting the regions of policy outside of deadband to zero:
            JointPol_Off2On(indPolDB_Off2On) == 0
            JointPol_On2Off(indPolDB_On2Off) == 0
                        
        %enforceing the thermostat deadband constraint:
            nuZeroOff(1,M) == JointPol_Off2On(M)
            nuMatOff(indThrmPol_Off2On(1:end-1)) == JointPol_Off2On(indThrmPol_Off2On(2:end))
            nuZeroOn(1,1) == JointPol_On2Off(1)
            nuMatOn(indThrmPol_On2Off(1:end-1)) == JointPol_On2Off(indThrmPol_On2Off(2:end))

        %% dynamic system constraints:                                  
                    
        %TRANSITIONING UNLOCKED TO UNLOCKED:
        
        %indices:
            indA1 = 1; indA2 = p.nTime*M;
            indB1 = p.nTime*M*p.tau + 1; indB2 = p.nTime*M*(p.tau+1);
        %starting in off state:
            nuMatOff(indA1:indA2) - JointPol_Off2Off*P_Off2Off - [nuZeroOff(p.tau+1,:),nuMatOff(indB1:indB2-M)]*P_Off2Off_TS == 0
        %starting in on state:
            nuMatOn(indA1:indA2) - JointPol_On2On*P_On2On - [nuZeroOn(p.tau+1,:),nuMatOn(indB1:indB2-M)]*P_On2On_TS == 0
        
        %TRANSITIONING LOCKED TO LOCKED:
            indA1 = p.nTime*M + 1; indA2 = p.nTime*M*2;
            tempBlockOne = [nuZeroOn(2,:),nuMatOn(indA1:indA2-M)];
            tempBlockTwo = [nuZeroOff(2,:),nuMatOff(indA1:indA2-M)];
            for kk = 3:1:(p.tau+1)

                %construct indicies for loop:
                indA1 = p.nTime*M*(kk-1) + 1;
                indA2 = p.nTime*M*kk;
                indB1 = indA1 - p.nTime*M;
                indB2 = indA2 - p.nTime*M;

                %starting in off state:
                nuMatOff(indA1:indA2) - [nuZeroOff(kk-1,:),nuMatOff(indB1:indB2-M)]*P_Off2Off_TS == 0
                %starting in on state:
                nuMatOn(indA1:indA2) - [nuZeroOn(kk-1,:),nuMatOn(indB1:indB2-M)]*P_On2On_TS == 0

                %block for the last set of dynamic constraints:
                tempBlockOne = tempBlockOne + [nuZeroOn(kk,:),nuMatOn(indA1:indA2-M)];
                tempBlockTwo = tempBlockTwo + [nuZeroOff(kk,:),nuMatOff(indA1:indA2-M)];

            end
        
        %TRANSITIONING UNLOCKED TO LOCKED:
            indA1 = p.nTime*M + 1; indA2 = p.nTime*M*2;
        %starting in off state:
            nuMatOff(indA1:indA2) - JointPol_On2Off*P_On2Off - tempBlockOne*P_On2Off_TS == 0
        %starting in on state:
            nuMatOn(indA1:indA2) - JointPol_Off2On*P_Off2On - tempBlockTwo*P_Off2On_TS == 0
                                 
    cvx_end  
        
%% compute policy from solution of cvx:

%reshape output of cvx:
    nuMatOn_R = reshape(nuMatOn',M*p.nTime,p.tau+1)';
    nuMatOff_R = reshape(nuMatOff',M*p.nTime,p.tau+1)';
    nuMatOn_R_noL = reshape(nuMatOn_R(1,:)',M,p.nTime)';
    nuMatOff_R_noL = reshape(nuMatOff_R(1,:)',M,p.nTime)';
    nuMat = [nuMatOff_R_noL,nuMatOn_R_noL];
    JointPol_On2Off = reshape(JointPol_On2Off',M,p.nTime)';
    JointPol_Off2On = reshape(JointPol_Off2On',M,p.nTime)';
    
    polEqConst_On2Off = zeros(M,1);
    polEqConst_On2Off(1) = 1;
    polEqConst_Off2On = zeros(M,1);
    polEqConst_Off2On(end) = 1;
    
%extract the policies:
    polOff2OnSave = zeros(p.numCVs+1,p.nTime);
    polOn2OffSave = zeros(p.numCVs+1,p.nTime);
    nuMatExtract = [nuZero(1,:);nuMat(1:end-1,:)];
    
    for jj=1:1:p.nTime 

        nuSelOff = nuMatExtract(jj,1:p.numCVs+1);
        nuSelOn = nuMatExtract(jj,p.numCVs+2:end); 

        diagNuMat = diag(1./nuSelOff);
        polOff2On = diag(diagNuMat*diag(JointPol_Off2On(jj,:)));
        polOff2On( JointPol_Off2On(jj,:) < 1E-5 ) = polEqConst_Off2On(JointPol_Off2On(jj,:) < 1E-5);
        polOff2OnSave(:,jj) = polOff2On;

        diagNuMat = diag(1./nuSelOn);
        polOn2Off = diag(diagNuMat*diag(JointPol_On2Off(jj,:)));
        polOn2Off(JointPol_On2Off(jj,:) < 1E-5) = polEqConst_On2Off(JointPol_On2Off(jj,:) < 1E-5);
        polOn2OffSave(:,jj) = polOn2Off;
    
    end    
 
%set output of model:
    gammaOut = C_R*nuMatOn';
    
%reset initial condition for moving horizon solver:  
    nuMatOnEnd = nuMatOn_R(:,end-M+1:end);
    nuMatOffEnd = nuMatOff_R(:,end-M+1:end);
    nuZero = [nuMatOffEnd,nuMatOnEnd];
    
end









