% function to convert temperature in Celsius to Fahrenheit
% syntax:
%
% function f = func_c2f(c)
%
% 1 input: c - temperature in Celsius, can be scalar or vector
% 1 output: f - temperature in Fahrenheit, same size as input
%
% ver 1: Jonathan Brooks

function f = func_c2f(c)
f = 9/5*c + 32;
