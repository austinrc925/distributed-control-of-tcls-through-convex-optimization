%Austin Coffman
%test script to run a simulation of TCLs with cvx computed policies. Can
%handle heterogeneity and time varying ambient conditions.

%clear interface:
    clear
    close all
    clc
    
%printing properties:
    printFig = 0;
    printDir = '/home/austin/Dropbox (UFL)/WorkingPapers/21_Coffman_FokkerPlankJournal/figures/';
    
%define parameters:
    loadData = 1;
    if (loadData == 1)
        load('optimalPolicies_ProblemData_2.mat')
%         load('optimalPolicies_ProblemData_3.mat')
        polOn2OffSave(isinf(polOn2OffSave) == 1) = 0;
        polOff2OnSave(isinf(polOff2OnSave) == 1) = 0;
        s_in = p;
        numDevices = 20000;
        gammaSave = [gammaSave,gammaSave(end)];
    else
        %time parameters:
        numDays = 1;
        numHours = 24*numDays;
        Ts = (1/2)*60/3600; %units: hour
        tHour = 0:Ts:numHours;
        nTime = length(tHour);
        numDevices = 30000;
        %ambient temperature:
        s_in.nPlan = 1;
        TsTa = 5*60/3600;
        tHourTa = 0:TsTa:numHours;
        TaRaw = designTA('WU',length(tHourTa),s_in,0,0,1,1);
        Ta = interp1(tHourTa,TaRaw(1:end-1),tHour); 
%         Ta = 30*ones(size(Ta));
        %define parameters for discretization:
    %finite volume method parameters:
        s_in.numCVs = 25;
        s_in.Tlow = 18; s_in.Thigh = 24;
        s_in.Tmax = 22; s_in.Tmin = 20;
        s_in.deltaT = (s_in.Tmax - s_in.Tmin)/s_in.numCVs;
        s_in.numBuffers = (s_in.Thigh - s_in.Tmax)/s_in.deltaT;
        s_in.numCVs = s_in.numCVs + s_in.numBuffers;
        s_in.deltaTime = Ts;
    %TCL modeling parameters:
        s_in.Cth = 2;
        s_in.Rth = 1;
        s_in.tempSet = (s_in.Tmax - s_in.Tmin)./2 + s_in.Tmin;
        s_in.rateP = (35 - s_in.tempSet)./s_in.Rth;
        s_in.sigmaSq = 0.05;
        s_in.bsize = s_in.numCVs + 1;
        
%         nuZero = 
    end
    
%     controlType = 'testRando';
    controlType = 'RandoControl';
%     controlType = 'ThermoStat';
    timeIntScheme = 'explicit';
%     timeIntScheme = 'implicit';
           
%define population parameters:
    s_in.nTime = nTime; s_in.numDevices = numDevices; s_in.T_s = Ts; s_in.tHour = tHour;
    s_in.COP = 2.5;
    s_in.C = s_in.Cth + 0*rand(s_in.numDevices,1);
    s_in.R = s_in.Rth + 0*rand(s_in.numDevices,1);
    s_in.stateSet = s_in.tempSet + 0*rand(s_in.numDevices,1);
    s_in.stateDelta = (s_in.Tmax - s_in.tempSet) + 0*ones(s_in.numDevices,1);
    s_in.tmprMax = s_in.stateSet + s_in.stateDelta;   
    s_in.tmprMin = s_in.stateSet - s_in.stateDelta;
    s_in.P = (35 - s_in.stateSet)./s_in.R;
    s_in.A = exp(-s_in.T_s./(s_in.C.*s_in.R));
%     s_in.sigmaSq = 0;
    s_in.var = s_in.sigmaSq*Ts;
    TaHist = repmat(Ta,numDevices,1);
        
%eq point value:
    pStar = (TaHist(1,:)'-s_in.stateSet(1))./(s_in.COP*s_in.R(1));
    yStar = pStar/(s_in.P(1)./s_in.COP);
    
%initial state (temperature/mode) for the population:
    tempVec = s_in.stateSet + 2*s_in.stateDelta.*rand(s_in.numDevices,1) - 1*s_in.stateDelta;
    modeVec = zeros(s_in.numDevices,1);%randi([0 1],s_in.numDevices,1);
    modeVec(rand(s_in.numDevices,1) < 0.4) = 1;
    
%bin the initial state:
    %the on state:
    BinCentersOn = (s_in.Tmin - s_in.deltaT/2):s_in.deltaT:(s_in.Thigh - s_in.deltaT/2);
    RightEdges = BinCentersOn + s_in.deltaT/2;
    LeftEdges = BinCentersOn - s_in.deltaT/2;
    BinEdgesOn = [LeftEdges,RightEdges(end)]';
    %the off state:
    BinCentersOff = (s_in.Tlow + s_in.deltaT/2):s_in.deltaT:(s_in.Tmax + s_in.deltaT/2);
    RightEdges = BinCentersOff + s_in.deltaT/2;
    LeftEdges = BinCentersOff - s_in.deltaT/2;
    BinEdgesOff = [LeftEdges,RightEdges(end)]';
    stateVectorOn = tempVec(modeVec == 1);
    stateVectorOff = tempVec(modeVec == 0);
    binStateOn = histcounts(stateVectorOn,BinEdgesOn);
    binStateOff = histcounts(stateVectorOff,BinEdgesOff);
%     nuZero = [binStateOff(:)',binStateOn(:)']./numDevices;
%     nuPrev = nuZero;
    bSize = s_in.numCVs + 1;
    p.tau = s_in.tau;
    nuPrev = zeros(1,2*(p.tau+1)*bSize);
    
    nuPrev(1:bSize) = 1*nuZero(1,1:bSize);
    nuPrev((p.tau+1)*bSize+1:(p.tau+1)*bSize+bSize) = nuZero(1,bSize+1:end);
%     nuPrev(2*bSize+1:3*bSize) = 1*nuZero(bSize+1:end);
%     nuPrev(3*bSize+1:end) = 0*nuZero(bSize+1:end);
    eyeMat = eye(length(nuPrev));
        
%prealloc:
    tmprHist = zeros(numDevices,nTime);
    modeHist = zeros(numDevices,nTime);
    natTranOn = zeros(1,nTime);
    natTranOff = zeros(1,nTime);
    deltaApprox = zeros(1,nTime);
    aggPowerHist = zeros(1,nTime);
    nuSave = zeros(nTime,bSize*2*(p.tau+1));
    natTranOnEst = zeros(1,nTime);
    natTranOffEst = zeros(1,nTime);
    muACC = 0;
    pieSave = zeros(nTime,bSize*2*(p.tau+1));
    onesVec = ones(1,bSize*2*(p.tau+1));
    onesMat = ones(bSize*2*(p.tau+1));
    powerSavePie = zeros(1,nTime);
    pSwitchVec_Off2On = zeros(numDevices,1);
    pSwitchVec_On2Off = zeros(numDevices,1);
    numSwitchOnModel = zeros(1,nTime-1);
    numSwitchOffModel = zeros(1,nTime-1);
    lockedStateSave = zeros(numDevices,nTime-1);
    cyclingTime = 'MA';
    if strcmp(cyclingTime,'MA')
        lockedStateMat = zeros(numDevices,p.tau);
    else
        lockedStateMat = zeros(numDevices,1);
        onesVecLock = ones(numDevices,1);
        pFlip = 0.4;
    end
    
    
%run simulation:
    for i=1:1:(nTime-1)
        
        %advance power consumption through markov model:
            s_in.thetaA = Ta(i);
            [Ptran,Arate,Pcf] = buildTranMatrix_withBuffer(s_in);
            if strcmp(controlType,'RandoControl')   
                pSwitch_On2Off = polOn2OffSave(:,i);
                pSwitch_Off2On = polOff2OnSave(:,i);
                Ptran = buildControlTranMatrix_movAvgLockOut(Pcf,pSwitch_On2Off,pSwitch_Off2On,p);
            elseif strcmp(controlType,'ThermoStat')
                pSwitch_Off2On = zeros(bSize,1);
                pSwitch_Off2On(end) = 1;
                pSwitch_On2Off = zeros(bSize,1);
                pSwitch_On2Off(1) = 1;  
                Ptran = buildControlTranMatrix_movAvgLockOut(Pcf,pSwitch_On2Off,pSwitch_Off2On,p);
            elseif strcmp(controlType,'testRando')
                %randomized control parameters:
                    pSwitch_Off2On = zeros(length(BinEdgesOff)-1,1);
                    pSwitch_Off2On(end) = 1;
                    pSwitch_Off2On(end-1) = 0.3;
                    pSwitch_Off2On(end-2) = 0.15;
                    pSwitch_Off2On(end-3) = 0.125;
                    pSwitch_Off2On(end-4) = 0.0625;
                    pSwitch_Off2On(end-5) = 0.0625/2;
                    pSwitch_On2Off = zeros(length(BinEdgesOn)-1,1);
                    pSwitch_On2Off(1) = 1;
                    pSwitch_On2Off(2) = 0.3;
                    pSwitch_On2Off(3) = 0.15;
                    pSwitch_On2Off(4) = 0.125;
                    pSwitch_On2Off(5) = 0.0625;
                    pSwitch_On2Off(6) = 0.0625/2;
                    Ptran = buildControlTranMatrix_movAvgLockOut(Pcf,pSwitch_On2Off,pSwitch_Off2On,p);               
            end
            
            if strcmp(timeIntScheme,'explicit')
                nuTilde = nuPrev*Ptran;
            elseif strcmp(timeIntScheme,'implicit')
                nuTilde = nuPrev/(2*eye(size(Ptran)) - Ptran);
            end
            
        %calculate number of switches:
            numSwitchOnModel(i) = nuPrev(1:bSize)*pSwitch_Off2On;             
            numSwitchOffModel(i) = nuPrev((p.tau+1)*bSize+1:(p.tau+1)*bSize+bSize)*pSwitch_On2Off;
            
            nuNext = nuTilde + muACC*(nuPrev-nuTilde);
            aggPowerHist(i) = numDevices*sum(nuPrev((p.tau+1)*bSize+1:end));
            nuSave(i,:) = nuPrev;
            nuPrev = nuNext;
            natTranOffEst(i) = nuPrev((p.tau+1)*bSize+1);
            natTranOnEst(i) = nuPrev(bSize);
            pieSave(i,:) = onesVec/(eye(size(Ptran)) - (Ptran - onesMat));
            powerSavePie(i) = numDevices*sum(pieSave(i,(p.tau+1)*bSize+1:end))*(s_in.P(1)./s_in.COP);
                        
        %save data:
            tmprHist(:,i) = tempVec;
            modeHist(:,i) = modeVec;
            modeVecTempSum = sum(modeVec);    
            
                       
            modeVecNow = modeVec;
        %reset control logic:
            if strcmp(controlType,'ThermoStat')
                
                natTranOn(i) = sum(tempVec(modeVec==0) > s_in.tmprMax(modeVec==0));
                natTranOff(i) = sum(tempVec(modeVec==1) < s_in.tmprMin(modeVec==1));
                
                modeVec(tempVec > s_in.tmprMax) = 1;
                modeVec(tempVec < s_in.tmprMin) = 0;
                
            elseif strcmp(controlType,'RandoControl') || strcmp(controlType,'testRando')
                
                lockedStateSum = sum(lockedStateMat,2);
                lockedStateSave(:,i) = lockedStateSum;
                
                natTranOn(i) = sum((tempVec(modeVec==0) > s_in.tmprMax(modeVec==0)).*(lockedStateSum(modeVec==0) == 0));
                natTranOff(i) = sum((tempVec(modeVec==1) < s_in.tmprMin(modeVec==1)).*(lockedStateSum(modeVec==1) == 0));
                
                IndCurrStateOn = find(modeVec == 1);
                IndCurrStateOff = find(modeVec == 0);
                
                stateIDOn = discretize(tempVec(IndCurrStateOn),BinEdgesOn);
                stateIDOff = discretize(tempVec(IndCurrStateOff),BinEdgesOff);
                
                indNANOn = find(isnan(stateIDOn));
                indNANOff = find(isnan(stateIDOff));
                
                stateIDOn(indNANOn) = [];
                IndCurrStateOn(indNANOn) = [];
                
                IndCurrStateOff(indNANOff) = [];
                stateIDOff(indNANOff) = [];
                
                
                pSwitchVec_On2Off = 0*pSwitchVec_On2Off;
                pSwitchVec_On2Off(IndCurrStateOn) = pSwitch_On2Off(stateIDOn);
                pSwitchVec_On2Off(lockedStateSum>0) = 0; 
                
                pSwitchVec_Off2On = 0*pSwitchVec_Off2On;
                pSwitchVec_Off2On(IndCurrStateOff) = pSwitch_Off2On(stateIDOff);
                pSwitchVec_Off2On(lockedStateSum>0) = 0;
                
                coinFlip = rand(numDevices,1);
                
                numSwitchOnTrue(i) = sum((coinFlip <= pSwitchVec_Off2On));
                numSwitchOffTrue(i) = sum(coinFlip <= pSwitchVec_On2Off);
                
                modeVec( logical((coinFlip <= pSwitchVec_Off2On)) ) = 1;
                modeVec( logical((coinFlip <= pSwitchVec_On2Off)) ) = 0;
                                
                modeVec( logical((tempVec > s_in.tmprMax)) ) = 1;
                modeVec( logical((tempVec < s_in.tmprMin)) ) = 0;
                
                %lock the devices:
                    if strcmp(cyclingTime,'MA')
                        lockedStateMat = [lockedStateMat(:,2:end),lockedStateMat(:,1)];
                        lockedStateMat(:,end) = 0;
%                         lockedStateMat(logical((coinFlip <= pSwitchVec_Off2On).*(lockedStateSum == 0)),end) = 1;
%                         lockedStateMat(logical((coinFlip <= pSwitchVec_On2Off).*(lockedStateSum == 0)),end) = 1;
                        lockedStateMat(:,end) = abs(modeVec-modeVecNow);
%                         lockedStateSave(:,i) = lockedStateMat(:,end);
                    else
                        
                        
                        coinFlip = rand(size(onesVecLock));
                        lockedStateMat(logical((coinFlip <= pFlip).*((lockedStateMat==1)) )) = 0;
                        lockedStateMat(lockedStateMat==0) = abs(modeVec(lockedStateMat==0)-modeVecNow(lockedStateMat==0));
%                         lockedStateSave(:,i) = lockedStateMat;
                    end
%                     lockedStateMat(logical((tempVec < s_in.tmprMin)),end) = 1;
%                     lockedStateMat(logical((tempVec > s_in.tmprMax)),end) = 1;
                     

            end
            
            modeVecTempSumTwo = sum(modeVec);
                                     %advance simulation:
            s_in.Ta = TaHist(:,i);
            tempVec = simPopulation(tempVec,modeVecNow,s_in);  
                           
        %calculate natTranOn/natTranOff:
            deltaApprox(i) = modeVecTempSumTwo - modeVecTempSum;
                        
    end
    
    %save marginals:
        if strcmp(controlType,'testRando')
            save('margNomRand.mat','nuSave');
        end
    
    
    
%%%%%%%%%%%Make plots:    
    
    %plot of natural switches on:
    figNatSwitchOnPlots = figure;
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    plot(tHour,natTranOn,'*--','Linewidth',0.75,'MarkerSize',0.75,'color',[0,0.8,0.3]); 
    hold on; 
    plot(tHour,numDevices.*natTranOnEst,'b','Linewidth',2)
    xlim([tHour(1),tHour(end)]);
    xticks(linspace(tHour(1),tHour(end),7));
    xlabel('Time (hours)');
    ylabel('Count');
    hLeg = legend('$ \bf{ D^{on}_k } $','$ \bf{ \hat{D}^{on}_k } $','Location','Best');
    hLeg.Interpreter = 'Latex';
    hLeg.Orientation = 'Horizontal';
    rect = [0.525, 0.9, 0.05, 0.05];
    hLeg.Position = rect;
    set(gca,'FontSize',12,'FontWeight','Bold');
    hLeg.FontSize = 16;
    if printFig
        pos = get(figNatSwitchOnPlots,'paperposition');
        set(figNatSwitchOnPlots,'paperposition',[0.01,-1.5,5,4]);
        set(figNatSwitchOnPlots,'PaperSize',[5,2.65]);
        print(figNatSwitchOnPlots,[printDir,'natOnSwitchCompare_withCycleState.pdf'],'-dpdf');
    end
    
    %plot of natural switches off:
    figNatSwitchOffPlots = figure;
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    plot(tHour,natTranOff,'*--','Linewidth',0.75,'MarkerSize',0.75,'color',[0,0.8,0.3]); 
    hold on; 
    plot(tHour,numDevices.*natTranOffEst,'b','Linewidth',2)
    xlim([tHour(1),tHour(end)]);
    xticks(linspace(tHour(1),tHour(end),7));
    xlabel('Time (hours)');
    ylabel('Count');
     ylim([0.8*min(numDevices.*natTranOffEst), 1.05*max(numDevices.*natTranOffEst)]);
    hLeg = legend('$ \bf{ D^{off}_k } $','$ \bf{ \hat{D}^{off}_k } $','Location','Best');
    hLeg.Interpreter = 'Latex';
    hLeg.Orientation = 'Horizontal';
    rect = [0.625, 0.9, 0.05, 0.05];
    hLeg.Position = rect;
    set(gca,'FontSize',12,'FontWeight','Bold');
    hLeg.FontSize = 16;
    
    if printFig
        pos = get(figNatSwitchOffPlots,'paperposition');
        set(figNatSwitchOffPlots,'paperposition',[0.01,-1.5,5,4]);
        set(figNatSwitchOffPlots,'PaperSize',[5,2.65]);
        print(figNatSwitchOffPlots,[printDir,'natOffSwitchCompare_withCycleState.pdf'],'-dpdf');
    end
     
    %plot of power consumption compare:
    figPowerPlot = figure;
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    plot(tHour,aggPowerHist*(s_in.P(1)./s_in.COP),'k','Linewidth',1.5);
    hold on;
    plot(tHour,sum(modeHist,1)*(s_in.P(1)./s_in.COP),'r--','Linewidth',1.75);
    xlim([tHour(1),tHour(end-2)]);
    ylim([0.95*min(aggPowerHist*(s_in.P(1)./s_in.COP)), 1.05*max(aggPowerHist*(s_in.P(1)./s_in.COP))]);
    hLeg = legend('$\bf{\gamma_k^{E}}$','$\bf{Y_k} $','Location','Best');
    hLeg.Interpreter = 'Latex';
    hLeg.Orientation = 'Vertical';
    rect = [0.825, 0.7, 0.05, 0.2];
    hLeg.Position = rect;
    xticks(linspace(tHour(1),tHour(end),7));
    xlabel('Time (hours)');
    ylabel('Power (kW)');
    set(gca,'FontSize',12,'FontWeight','Bold');
    hLeg.FontSize = 16;
    hLeg.Box = 'off';
    
    if printFig
        pos = get(figPowerPlot,'paperposition');
        set(figPowerPlot,'paperposition',[0.01,-1.4,5,4]);
        set(figPowerPlot,'PaperSize',[5,2.65]);
        print(figPowerPlot,[printDir,'powerTrackingModelCompare_Cycle.pdf'],'-dpdf');
    end
    
    %plot showing the policies used for evaluating aggregate model:
        if strcmp(controlType,'testRando')
            polPlotString = 'testPolicyCompare.pdf';
            polLabelOne = '$\bf{\phi^{GS}_{off}}$';
            polLabelTwo = '$\bf{\phi^{GS}_{on}}$';
        elseif strcmp(controlType,'RandoControl')
            polPlotString = 'testPolicyCompare_optPol.pdf';
            polLabelOne = '$\bf{\phi^{GS,*}_{off}}$';
            polLabelTwo = '$\bf{\phi^{GS,*}_{on}}$';
        end    
    
        figTestPolComp = figure;
        axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
        axes(axA)
        plot(BinCentersOff,polOff2OnSave(:,500),'ro-','LineWidth',1.5,'MarkerSize',8.5);
        hold on;
        plot(BinCentersOn,polOn2OffSave(:,500),'b*-','LineWidth',1.5,'MarkerSize',8.5);
        xlim([0.99*min(s_in.tmprMin),1.01*max(s_in.tmprMax)])
        xticks([20,21,22])
        xticklabels({'\lambda^{min}','\lambda^{set}','\lambda^{max}'});
        hLeg = legend(polLabelOne,polLabelTwo);
        hLeg.Interpreter = 'Latex';
        hLeg.Orientation = 'Vertical';
        rect = [0.525, 0.7, 0.05, 0.2];
        hLeg.Position = rect;
        xlabel('Temperature ^\circC');
        ylabel('Prob.');
        set(gca,'FontSize',12,'FontWeight','Bold');
        hLeg.FontSize = 16;
        hLeg.Box = 'off';
            
        if printFig
            pos = get(figTestPolComp,'paperposition');
            set(figTestPolComp,'paperposition',[0.01,-1.4,5,4]);
            set(figTestPolComp,'PaperSize',[5,2.65]);
            print(figTestPolComp,[printDir,polPlotString],'-dpdf');
        end
        
        
        
        
    %plot of reference tracking:
    if strcmp(controlType,'RandoControl')
        figPowerModelPlot = figure;
        axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
        axes(axA)
        plot(tHour,pStar*numDevices,'b','Linewidth',1.5);
        hold on;
        plot(tHour,numDevices*gammaSave*(s_in.P(1)./s_in.COP),'k','Linewidth',1.5);
        plot(tHour,sum(modeHist,1)*(s_in.P(1)./s_in.COP),'r--','Linewidth',1.5);
        xlim([tHour(1),tHour(end)]);
        ylim([0.95*min(aggPowerHist*(s_in.P(1)./s_in.COP)), 1.05*max(aggPowerHist*(s_in.P(1)./s_in.COP))]);
        hLeg = legend('$\bf{\bar{P}_k}$','$ \bf{ r_k } $','$ \bf{ Y_k } $','Location','Best');
        hLeg.Interpreter = 'Latex';
        hLeg.Orientation = 'Vertical';
        hLeg.Box = 'off';
        rect = [0.825, 0.8, 0.05, 0.05];
        hLeg.Position = rect;
        xticks(linspace(tHour(1),tHour(end),7));
        xlabel('Time (hours)');
        ylabel('Power (kW)');
        set(gca,'FontSize',12,'FontWeight','Bold');
        hLeg.FontSize = 16;
        if printFig
            pos = get(figPowerModelPlot,'paperposition');
            set(figPowerModelPlot,'paperposition',[0.01,-1.5,5,4]);
            set(figPowerModelPlot,'PaperSize',[5,2.65]);
            print(figPowerModelPlot,[printDir,'powerReferenceTracking.pdf'],'-dpdf');
        end
    end
    
    %plot of baseline power consumption compare:
    figBassCompare = figure;
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    plot(tHour,pStar*numDevices,'b','Linewidth',1.5);
    hold on;
    plot(tHour,refSignal*numDevices*(s_in.P(1)./s_in.COP),'k','Linewidth',1.5);
    hold on;
    plot(tHour,numDevices*gammaSave*(s_in.P(1)./s_in.COP),'r--','Linewidth',1.5);
    xlim([tHour(1),tHour(end)]);
    ylim([0.95*min(aggPowerHist*(s_in.P(1)./s_in.COP)), 1.05*max(aggPowerHist*(s_in.P(1)./s_in.COP))]);
    hLeg = legend('$\bf{\bar{P}_k}$','$ \bf{ r^{BA}_k } $','$ \bf{ r_k } $','Location','Best');
    hLeg.Interpreter = 'Latex';
    hLeg.Orientation = 'Vertical';
    hLeg.Box = 'off';
    rect = [0.825, 0.8, 0.05, 0.05];
    hLeg.Position = rect;
    xticks(linspace(tHour(1),tHour(end),7));
    xlabel('Time (hours)');
    ylabel('Power (kW)');
    set(gca,'FontSize',12,'FontWeight','Bold');
    hLeg.FontSize = 16;
    
    if printFig
        pos = get(figBassCompare,'paperposition');
        set(figBassCompare,'paperposition',[0.01,-1.5,5,4]);
        set(figBassCompare,'PaperSize',[5,2.65]);
        print(figBassCompare,[printDir,'solOptProb_refSig.pdf'],'-dpdf');
    end
      
        %bin state using histogram command:
        indexPlotVec = [400,600,750,1000,1400];
        plotType = 'Locked';
        for tt=1:1:length(indexPlotVec)
            indexPlot = indexPlotVec(tt);
            tempVecEnd = tmprHist(:,indexPlot);
            modeVecEnd = modeHist(:,indexPlot);
            lockedStateEnd = lockedStateSave(:,indexPlot);
            
            if strcmp(plotType,'notLocked')

                stateVectorOn = tempVecEnd(logical( (modeVecEnd == 1).*(lockedStateEnd == 0) ));
                stateVectorOff = tempVecEnd(logical( (modeVecEnd == 0).*(lockedStateEnd == 0) ));

                nuPlotOn = nuSave(indexPlot,(p.tau+1)*bSize+1:(p.tau+1)*bSize+bSize);
                nuPlotOff = nuSave(indexPlot,1:1*bSize);

                piePlotOn = pieSave(indexPlot,(p.tau+1)*bSize+1:(p.tau+1)*bSize+bSize);
                piePlotOff = pieSave(indexPlot,1:1*bSize);

            elseif strcmp(plotType,'both')

                stateVectorOn = tempVecEnd(logical( (modeVecEnd == 1)));
                stateVectorOff = tempVecEnd(logical( (modeVecEnd == 0))); 

                nuLockedOn = reshape( nuSave(indexPlot,(p.tau+1)*bSize+1:end), bSize, p.tau+1);
                nuPlotOn = sum(nuLockedOn,2)';

                pieLockedOn = reshape( pieSave(indexPlot,(p.tau+1)*bSize+1:end), bSize, p.tau+1);
                piePlotOn = sum(pieLockedOn,2)';

                nuLockedOff = reshape( nuSave(indexPlot,1:(p.tau+1)*bSize), bSize, p.tau+1);
                nuPlotOff = sum(nuLockedOff,2)';

                pieLockedOff = reshape( pieSave(indexPlot,1:(p.tau+1)*bSize), bSize, p.tau+1);
                piePlotOff = sum(pieLockedOff,2)';            


            else

                stateVectorOn = tempVecEnd(logical( (modeVecEnd == 1).*(lockedStateEnd == 1) ));
                stateVectorOff = tempVecEnd(logical( (modeVecEnd == 0).*(lockedStateEnd == 1) ));

                nuLockedOn = reshape( nuSave(indexPlot,(p.tau+1)*bSize+bSize+1:end), bSize, p.tau);
                nuPlotOn = sum(nuLockedOn,2)';
%                 nuPlotOn = unLockedOne(:,3);
                
                pieLockedOn = reshape( pieSave(indexPlot,(p.tau+1)*bSize+bSize+1:end), bSize, p.tau);
                piePlotOn = sum(pieLockedOn,2)';

                nuLockedOff = reshape( nuSave(indexPlot,bSize+1:(p.tau+1)*bSize), bSize, p.tau);
                nuPlotOff = sum(nuLockedOff,2)';

                pieLockedOff = reshape( pieSave(indexPlot,bSize+1:(p.tau+1)*bSize), bSize, p.tau);
                piePlotOff = sum(pieLockedOff,2)';

            end
            
            %save results for plotting:
            stateVectorOnSave.(['plot_',num2str(tt)]) = stateVectorOn;
            nuPlotOnSave.(['plot_',num2str(tt)]) = nuPlotOn;
            
        end
        binStateOn = histcounts(stateVectorOn,BinEdgesOn);
        binStateOff = histcounts(stateVectorOff,BinEdgesOff);
        
        %make multi-plot histogram:
        s_plot.nuPlotOn = nuPlotOnSave;
        s_plot.stateVectorOnSave = stateVectorOnSave;
        s_plot.BinCentersOn = BinCentersOn;
        s_plot.BinEdgesOn = BinEdgesOn;
        s_plot.numDevices = numDevices;
        figHist_multi = makeTimeHistPlot_cycleState(s_plot);
        printMultiHist = 0;
        
        if strcmp(plotType,'notLocked') && printMultiHist
            pos = get(figHist_multi,'paperposition');
            set(figHist_multi,'paperposition',[0.01,-1.4,5,4]);
            set(figHist_multi,'PaperSize',[5,2.5]);
            if strcmp(controlType,'testRando')
                print(figHist_multi,[printDir,'histTrackingCentralized_withCycleState_notLocked.pdf'],'-dpdf');
            elseif strcmp(controlType,'RandoControl')
                print(figHist_multi,[printDir,'histTrackingCentralized_withCycleState_notLocked_optPol.pdf'],'-dpdf');
            end
        elseif strcmp(plotType,'Locked') && printMultiHist
            pos = get(figHist_multi,'paperposition');
            set(figHist_multi,'paperposition',[0.01,-1.4,5,4]);
            set(figHist_multi,'PaperSize',[5,2.5]);
            if strcmp(controlType,'testRando')
                print(figHist_multi,[printDir,'histTrackingCentralized_withCycleState_Locked.pdf'],'-dpdf');
            elseif strcmp(controlType,'RandoControl')
                print(figHist_multi,[printDir,'histTrackingCentralized_withCycleState_Locked_optPol.pdf'],'-dpdf');
            end
        end
        
        
%         onShiftValue = 3;
%         figHist = figure; 
%         axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
%         axes(axA)
%         yMax = 1.15*max([nuPlotOn,nuPlotOff]*numDevices);
%         LB = zeros(size(BinCentersOff));
%         UB = yMax*ones(size(BinCentersOff));
%         hPatchOff = patch([BinCentersOff , fliplr(BinCentersOff)], [LB, fliplr(UB)], 'g', 'FaceAlpha',0.075);
%         hPatchOff.EdgeColor = [1,1,1];
%         hPatchOff.EdgeAlpha = 0;
%         hPatchOn = patch([BinCentersOn+onShiftValue , fliplr(BinCentersOn+onShiftValue)], [LB, fliplr(UB)], 'm', 'FaceAlpha',0.075);
%         hPatchOn.EdgeColor = [1,1,1];
%         hPatchOn.EdgeAlpha = 0;
%         hold on;
%         H1 = histogram(stateVectorOn+onShiftValue,BinEdgesOn+onShiftValue);
%         H1.FaceColor = [1 0 0];
%         H1.FaceAlpha = 0.15;
%         hold on;
%         H2 = histogram(stateVectorOff,BinEdgesOff);
%         H2.FaceColor = [0 0 1];
%         H2.FaceAlpha = 0.15;
%         xticks([linspace(18,22,5),linspace(20,24,5)+onShiftValue])
%         xticklabels([linspace(18,22,5),(linspace(20,24,5))]);
%         xlim([18,24+onShiftValue])
%         hold on; 
%         plot(BinCentersOn+onShiftValue,nuPlotOn*numDevices,'r','LineWidth',2);
% %         plot(BinCentersOn+onShiftValue,piePlotOn*numDevices,'m--','LineWidth',1.5);
%         plot(BinCentersOff,nuPlotOff*numDevices,'b','LineWidth',2);
% %         plot(BinCentersOff,piePlotOff*numDevices,'m--','LineWidth',1.5);
%         text(18.5,500,'Off','FontSize',14,'FontWeight','Bold');
%         text(24+onShiftValue -1,500,'On','FontSize',14,'FontWeight','Bold');
% %         ylim([0,yMax]);
%         ylabel('Count');
%         xlabel('Temperature ^\circ C')
%         set(gca,'FontSize',12,'FontWeight','Bold');
%         if printFig
%             pos = get(figHist,'paperposition');
%             set(figHist,'paperposition',[0.01,-1.4,5,4]);
%             set(figHist,'PaperSize',[5,2.5]);
%             print(figHist,[printDir,'histTrackingCentralized_withCycleState.pdf'],'-dpdf');
%         end
        
%make sparsity pattern plot:
%     LBOne = s_in.numBuffers*ones(1,s_in.numBuffers);
%     UBOne = zeros(1,s_in.numBuffers);
%     xValOne = linspace(1,s_in.numBuffers,s_in.numBuffers);
%     
%     xValTwo = linspace(s_in.numBuffers+1,s_in.numCVs+1,s_in.numBuffers+1);
%     LBTwo = (s_in.numCVs+1)*ones(1,s_in.numCVs-s_in.numBuffers+1);
%     UBTwo = (s_in.numBuffers+1)*ones(1,s_in.numCVs-s_in.numBuffers+1);
%     
%     xValThree = linspace(s_in.numCVs+2,s_in.numCVs+2+s_in.numBuffers,s_in.numBuffers+1);
%     LBThree = (s_in.numCVs+2+s_in.numBuffers)*ones(1,s_in.numBuffers+1);
%     UBThree = (s_in.numCVs+2)*ones(1,s_in.numBuffers+1);
%     
%     xValFour = linspace(s_in.numCVs+2+s_in.numBuffers+1,s_in.numCVs*2+2,s_in.numBuffers);
%     LBFour = (s_in.numCVs*2+2)*ones(1,s_in.numBuffers);
%     UBFour = (s_in.numCVs*2+2-s_in.numBuffers+1)*ones(1,s_in.numBuffers);
%     
%     
%     figSparPattern = figure;
%     axA = axes('position',[0.15    0.14   0.9    0.6]);
%     axes(axA)
%         
%     hPatchOff = patch([xValOne , fliplr(xValOne)], [LBOne, fliplr(UBOne)], 'g', 'FaceAlpha',0.1);
%     hPatchOff.EdgeColor = [1,1,1];
%     hPatchOff.EdgeAlpha = 0;
%     hold on;
%     
%     hPatchOff2 = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], 'b', 'FaceAlpha',0.1);
%     hPatchOff2.EdgeColor = [1,1,1];
%     hPatchOff2.EdgeAlpha = 0;
%     
%     hPatchOff3 = patch([xValThree , fliplr(xValThree)], [LBThree, fliplr(UBThree)], 'r', 'FaceAlpha',0.1);
%     hPatchOff3.EdgeColor = [1,1,1];
%     hPatchOff3.EdgeAlpha = 0;
%     
%     hPatchOff4 = patch([xValFour , fliplr(xValFour)], [LBFour, fliplr(UBFour)], 'm', 'FaceAlpha',0.1);
%     hPatchOff4.EdgeColor = [1,1,1];
%     hPatchOff4.EdgeAlpha = 0;
%     
%     xPlotOne = [xValOne,xValTwo];
%     yPlotOne = LBTwo(1)*ones(size(xPlotOne));
%     plot(xPlotOne,yPlotOne,'k--','LineWidth',0.75);
%     
%     xPlotTwo = xPlotOne(end)*ones(size(xPlotOne));
%     yPlotTwo = xPlotOne;
%     plot(xPlotTwo,yPlotTwo,'k--','LineWidth',0.75);
%     
%     xPlotThree = [xValThree,xValFour];
%     yPlotThree = yPlotOne+1;
%     plot(xPlotThree,yPlotThree,'k--','LineWidth',0.75);
%     
%     xPlotFour = xPlotThree(1)*ones(size(xPlotThree));
%     yPlotFour = xPlotThree;
%     plot(xPlotFour,yPlotFour,'k--','LineWidth',0.75);
%         
%     spy(Arate,'b',5);
%     
%     plot(s_in.numCVs+2+s_in.numBuffers+1,s_in.numCVs+1,'r.','MarkerSize',12);
%     plot(s_in.numBuffers,s_in.numCVs+2,'r.','MarkerSize',12);
%     
%     text(s_in.numBuffers+s_in.numCVs+2,-2,'On','FontSize',14,'FontName','Times');
%     text(s_in.numBuffers,-2,'Off','FontSize',14,'FontName','Times');
%     opacity = 0.6;
%     myColor = [0,0,1];
%     text(s_in.numCVs-24,s_in.numBuffers-4,'$\hat{A}_{off}(t)$','Interpreter','Latex','FontSize',12,'FontName','Times','Color',(1 - opacity * (1 - myColor)));
%     myColor = [1,0,0]; 
%     opacity = 0.5;
%     text(s_in.numCVs+27,2*s_in.numBuffers+9,'$\hat{A}_{on}(t)$','Interpreter','Latex','FontSize',12,'FontName','Times','Color',(1 - opacity * (1 - myColor)));
%     
%     xticks([s_in.numCVs+2,s_in.numCVs+2+s_in.numBuffers,2*s_in.numCVs+2]);
%     xticklabels({'\lambda^{min}','\lambda^{max}','\lambda^{high}'});
%     
%     yticks([1,1+s_in.numBuffers,s_in.numCVs+1]);
%     yticklabels({'\lambda^{low}','\lambda^{min}','\lambda^{max}'});
%     set(gca,'FontName','Times','FontSize',14);
%     if printFig
%         pos = get(figSparPattern,'paperposition');
%         set(figSparPattern,'paperposition',[-1,-0.14,4.5,3.75]);
%         set(figSparPattern,'PaperSize',[3,2.75]);
%         print(figSparPattern,[printDir,'sparPatternArate.pdf'],'-dpdf');
%     end
    
    totalSwitchTimes = [];
    tau = p.tau;
    indexTwo = 600;
    indexOne = 1;
    minSwitchTime = [];
    modeHistSwitchStatistics = modeHist(:,1:end-1);
    count = 1;
    badTCL = 0;
    for i=1:1:3000
        tempMode = modeHistSwitchStatistics(i,indexOne:end);
        tempSwitch = abs(diff(tempMode));
%         switchSave(i,:) = tempSwitch;
        tempSwitchTimes = diff(find(tempSwitch == 1)); %countSwitchTimes(tempSwitch);
        if min(tempSwitchTimes) < p.tau
            badTCL(count) = i;
            count = count + 1;
        end
        totalSwitchTimes = [totalSwitchTimes;tempSwitchTimes(:)];
%         avgSwitchTimes(i) = mean(tempSwitchTimes);
        minSwitchTime = [minSwitchTime;min(tempSwitchTimes)];
%         minOccurances(i) = length(find(tempSwitchTimes == minSwitchTime(i)));
        numSwitches(i) = length(tempSwitchTimes) + 1;
    end
    
%histogram of cycling QoS:
    figHistCycQoS = figure;
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    Hcyc = histogram(totalSwitchTimes);
    Hcyc.FaceColor = [1 0 0];
    Hcyc.FaceAlpha = 0.15;
    yMax = 1.1*max(Hcyc.Values);
    xPlotTau = p.tau*linspace(1,1,500);
    yPlotTau = linspace(1,yMax,500);
    hold on; 
    plot(xPlotTau,yPlotTau,'b--','LineWidth',1.5);
    xlim([0,50])
    ylim([0,yMax]);
    xlabel('Time Between Switches (Minutes)')
    ylabel('Count');
    set(gca,'FontSize',12,'FontWeight','Bold','FontName','Times');
    if 0
        pos = get(figHistCycQoS,'paperposition');
        set(figHistCycQoS,'paperposition',[0.01,-1.4,5,4]);
        set(figHistCycQoS,'PaperSize',[5,2.5]);
        print(figHistCycQoS,[printDir,'histCyclingQoS_withCycleState.pdf'],'-dpdf');
    end
        
%histogram of temperature QoS
    figHistTmprQoS = figure;
    numDevicesPlot = 200;
    tmprVecForHist = reshape(tmprHist(1:numDevicesPlot,:),numDevicesPlot*nTime,1);
    axA = axes('position',[0.15    0.14*3.5   0.8    0.45]);
    axes(axA)
    Htmpr = histogram(tmprVecForHist);
    Htmpr.FaceColor = [1 0 0];
    Htmpr.FaceAlpha = 0.15;
    yMax = 1.1*max(Htmpr.Values);
    xticks([20,21,22])
    xticklabels({'\lambda^{min}','\lambda^{set}','\lambda^{max}'});
    ylim([0,yMax]);
    xlim([s_in.Tmin*0.95,s_in.Tmax*1.05])
    yPlotTau = linspace(1,yMax,500);
    hold on; 
    plot(s_in.Tmin*linspace(1,1,500),yPlotTau,'b--','LineWidth',2.5);
    plot(s_in.Tmax*linspace(1,1,500),yPlotTau,'b--','LineWidth',2.5);
    xlabel('Temperature ( ^\circ C)')
    ylabel('Count');
    set(gca,'FontSize',12,'FontWeight','Bold','FontName','Times');
    if 0
        pos = get(figHistTmprQoS,'paperposition');
        set(figHistTmprQoS,'paperposition',[0.01,-1.4,5,4]);
        set(figHistTmprQoS,'PaperSize',[5,2.5]);
        print(figHistTmprQoS,[printDir,'histTmprQoS_withCycleState.pdf'],'-dpdf');
    end
    
%make sparsity pattern joint matrix:
    sqSize = bSize*(p.tau+1);
    horzLineX = linspace(0,2*sqSize,2*sqSize);
    horzLineY = sqSize*ones(1,2*sqSize);
    vertLineX = sqSize*ones(1,2*sqSize);
    vertLineY = linspace(0,2*sqSize,2*sqSize);
    
    figSparJoint = figure;
   
    %dividing lines:
    plot(horzLineX,horzLineY,'k','LineWidth',1.5);
    hold on;
    plot(vertLineX,vertLineY,'k','LineWidth',1.5);
    
    %sparsity pattern:
    spy(Ptran,'b',5);
    
    %patches:
        
        indOneX = [1,2*bSize+1,3*bSize+1,4*bSize+1,5*bSize+1,1];
        indTwoX = [bSize,3*bSize,4*bSize,5*bSize,6*bSize,bSize];
        indOneY = [bSize,2*bSize,3*bSize,4*bSize,5*bSize,6*bSize];
        indTwoY = [0,bSize,2*bSize,3*bSize,4*bSize,5*bSize];
        
    for jj=1:1:(p.tau+1)
        
        if jj == 1
            colorStr = 'r';
        else
            colorStr = 'g';
        end
        
        %patches off off quad:      
        xValTwo = linspace(indOneX(jj),indTwoX(jj),bSize);
        LBTwo = indOneY(jj)*ones(1,bSize);
        UBTwo = indTwoY(jj)*ones(1,bSize);
        hPatchOffOff = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], colorStr, 'FaceAlpha',0.1);
        hPatchOffOff.EdgeColor = [1,1,1];
        hPatchOffOff.EdgeAlpha = 0;
        
        %patches on on quad:
        xValTwo = linspace(indOneX(jj),indTwoX(jj),bSize) + sqSize;
        LBTwo = indOneY(jj)*ones(1,bSize) + sqSize;
        UBTwo = indTwoY(jj)*ones(1,bSize) + sqSize;
        hPatchOnOn = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], colorStr, 'FaceAlpha',0.1);
        hPatchOnOn.EdgeColor = [1,1,1];
        hPatchOnOn.EdgeAlpha = 0;
        
    end
    
        indOneX = [bSize+1,bSize+1,bSize+1,bSize+1,bSize+1,bSize+1];
        indTwoX = [2*bSize,2*bSize,2*bSize,2*bSize,2*bSize,2*bSize];
        indOneY = [bSize,2*bSize,3*bSize,4*bSize,5*bSize,6*bSize] + sqSize;
        indTwoY = [0,bSize,2*bSize,3*bSize,4*bSize,5*bSize] + sqSize;
        
    for jj=1:1:(p.tau+1)
    
        if jj == 1
            colorStr = 'r';
        else
            colorStr = 'g';
        end
        
        %patches on off quad:
        xValTwo = linspace(indOneX(jj),indTwoX(jj),bSize);
        LBTwo = indOneY(jj)*ones(1,bSize);
        UBTwo = indTwoY(jj)*ones(1,bSize);
        if jj == 1
            hPatchOffOffLeg = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], colorStr, 'FaceAlpha',0.1);
            hPatchOffOffLeg.EdgeColor = [1,1,1];
            hPatchOffOffLeg.EdgeAlpha = 0;
        else
            hPatchOffOff = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], colorStr, 'FaceAlpha',0.1);
            hPatchOffOff.EdgeColor = [1,1,1];
            hPatchOffOff.EdgeAlpha = 0;
        end
        %patches off on quad:
        xValTwo = linspace(indOneX(jj),indTwoX(jj),bSize) + sqSize;
        LBTwo = indOneY(jj)*ones(1,bSize) - sqSize;
        UBTwo = indTwoY(jj)*ones(1,bSize) - sqSize;
        hPatchOffOff = patch([xValTwo , fliplr(xValTwo)], [LBTwo, fliplr(UBTwo)], colorStr, 'FaceAlpha',0.1);
        hPatchOffOff.EdgeColor = [1,1,1];
        hPatchOffOff.EdgeAlpha = 0;
    
    end
    
    text(sqSize/2 - 15,-15,'Off','FontSize',14,'FontName','Times');
    text(sqSize/2 + sqSize - 15,-15,'On','FontSize',14,'FontName','Times');
    text(-65,bSize/2,'l = 0','FontSize',14,'FontName','Times');
    text(-65,bSize/2 + bSize,'l = 1','FontSize',14,'FontName','Times');
    text(-30,bSize/2 + 2*bSize,'.','FontSize',16,'FontName','Times');
    text(-30,1.2*(bSize/2 + 2*bSize),'.','FontSize',16,'FontName','Times');
    text(-30,1.4*(bSize/2 + 2*bSize),'.','FontSize',16,'FontName','Times');
    text(-65,sqSize - bSize/2,'l = \tau','FontSize',14,'FontName','Times');
    xticks([])
    yticks([])
    set(gca,'FontSize',12,'FontWeight','Bold');
    hleglines = [hPatchOffOffLeg, hPatchOffOff];
    hLeg = legend(hleglines,'$\bf{with \ \phi^{GS}} \hspace{5mm}$','$\bf{with \ \phi^{TS}}$');
    hLeg.Interpreter = 'Latex';
    hLeg.Orientation = 'Horizontal';
    hLeg.Position = [0.48,0.05,0.05,0.05];
    hLeg.Box = 'Off';
    figSparJoint.CurrentAxes.XLabel.String = '';
    if printFig
        pos = get(figSparJoint,'paperposition');
        set(figSparJoint,'paperposition',[-0.4,-0.1,4.5,3.75]);
        set(figSparJoint,'PaperSize',[3.75,3.75]);
        print(figSparJoint,[printDir,'sparPatternPtranCycle.pdf'],'-dpdf');
    end
    
    
    
    
    
    
        
        
    
    